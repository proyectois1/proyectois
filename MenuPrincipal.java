
package proyectois;

import javax.swing.JOptionPane;


public class MenuPrincipal extends javax.swing.JFrame {

    public MenuPrincipal() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSpinner1 = new javax.swing.JSpinner();
        BTNproveedor = new javax.swing.JButton();
        BTNproducto = new javax.swing.JButton();
        BTNsalir = new javax.swing.JButton();
        BTNcliente = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        BTNproveedor1 = new javax.swing.JButton();
        BTNproducto1 = new javax.swing.JButton();
        BTNsalir1 = new javax.swing.JButton();

        BTNproveedor.setText("GESTIÓN DE PROVEEDORES");

        BTNproducto.setText("GESTIÓN DE PRODUCTOS");

        BTNsalir.setText("SALIR");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        BTNcliente.setText("GESTIÓN DE CLIENTES");
        BTNcliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNclienteActionPerformed(evt);
            }
        });

        jLabel1.setText("  MENÚ PRINCIPAL");

        BTNproveedor1.setText("GESTIÓN DE PROVEEDORES");
        BTNproveedor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNproveedor1ActionPerformed(evt);
            }
        });

        BTNproducto1.setText("GESTIÓN DE PRODUCTOS");
        BTNproducto1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNproducto1ActionPerformed(evt);
            }
        });

        BTNsalir1.setText("SALIR");
        BTNsalir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNsalir1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BTNsalir1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BTNproveedor1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BTNcliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BTNproducto1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BTNcliente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BTNproveedor1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BTNproducto1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BTNsalir1)
                .addGap(32, 32, 32))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BTNclienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNclienteActionPerformed
        MenuClientes m = new MenuClientes();
        m.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BTNclienteActionPerformed

    private void BTNproveedor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNproveedor1ActionPerformed
        MenuProveedores m = new MenuProveedores();
        m.setVisible(true);
        this.dispose();// TODO add your handling code here:
    }//GEN-LAST:event_BTNproveedor1ActionPerformed

    private void BTNproducto1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNproducto1ActionPerformed
        MenuProductos m = new MenuProductos();
        m.setVisible(true);
        this.dispose();// TODO add your handling code here:
    }//GEN-LAST:event_BTNproducto1ActionPerformed

    private void BTNsalir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNsalir1ActionPerformed
        int a= JOptionPane.showConfirmDialog(null,"¿DESEA SALIR?");
        if(a==JOptionPane.YES_OPTION)
            System.exit(0);
    }//GEN-LAST:event_BTNsalir1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTNcliente;
    private javax.swing.JButton BTNproducto;
    private javax.swing.JButton BTNproducto1;
    private javax.swing.JButton BTNproveedor;
    private javax.swing.JButton BTNproveedor1;
    private javax.swing.JButton BTNsalir;
    private javax.swing.JButton BTNsalir1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JSpinner jSpinner1;
    // End of variables declaration//GEN-END:variables
}
