package proyectois;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;
import javax.swing.JOptionPane;

public class ProductoActualizar extends javax.swing.JFrame {

    public ProductoActualizar() {
        initComponents();
    }

    public void llenarcombo(ArrayList<String> f) {
        for (int i = 0; i < f.size(); i++) {
            comboActualizar.addItem(f.get(i));
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        comboActualizar = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jtxtID = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jtxtnom = new javax.swing.JTextField();
        jtxtcant = new javax.swing.JTextField();
        jtxtUM = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jtxtprec = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Seleccione cédula:");

        comboActualizar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "seleccione....." }));
        comboActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboActualizarActionPerformed(evt);
            }
        });

        jLabel2.setText("ID:");

        jtxtID.setEditable(false);

        jLabel3.setText("Nombre:");

        jLabel4.setText("Cantidad:");

        jLabel5.setText("Unidad de Medida:");

        jButton1.setText("ACTUALIZAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("CANCELAR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel6.setText("Precio:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(jButton2))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(comboActualizar, 0, 90, Short.MAX_VALUE)
                            .addComponent(jtxtID)
                            .addComponent(jtxtnom)
                            .addComponent(jtxtcant)
                            .addComponent(jtxtUM)
                            .addComponent(jtxtprec))))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jtxtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jtxtnom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jtxtcant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jtxtUM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jtxtprec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(34, 34, 34))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboActualizarActionPerformed
        if (comboActualizar.getSelectedIndex() == 0) {
            jtxtID.setText("");
            jtxtUM.setText("");
            jtxtnom.setText("");
            jtxtcant.setText("");
            jtxtprec.setText("");
        } else {
            AccesoBD a = new AccesoBD();

            try {
                ResultSet r;
                r = a.consultaBD("select producto_ID, producto_nom,producto_cant,producto_med,producto_prec from producto where producto_ID= '" + comboActualizar.getSelectedItem() + "'");
                while (r.next()) {
                    jtxtID.setText(r.getString("producto_ID"));
                    jtxtnom.setText(r.getString("producto_nom"));
                    jtxtcant.setText(r.getString("producto_cant"));
                    jtxtUM.setText(r.getString("producto_med"));
                    jtxtprec.setText(r.getString("producto_prec"));
                }
            } catch (Exception ex) {
                System.out.println(ex);
                a.cerrarBD();
            }
        }
    }//GEN-LAST:event_comboActualizarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (comboActualizar.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "debe seleccionar un ID");
        } else if (validacion()) {
            int b = JOptionPane.showConfirmDialog(null, "¿Seguro desea actualizar estos datos?");
            if (b == JOptionPane.YES_OPTION) {
                AccesoBD a = new AccesoBD();
                a.actualizaBD("update producto set producto_med='" + jtxtUM.getText() + "',producto_nom='" + jtxtnom.getText() + 
                        "',producto_cant='" + jtxtcant.getText() + "',producto_prec='"+ jtxtprec.getText()+"' where producto_id= '"+jtxtID.getText()+"'");
                MenuProductos m = new MenuProductos();
                a.cerrarBD();
                m.setVisible(true);
                this.dispose();
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int a = JOptionPane.showConfirmDialog(null, "¿DESEA CANCELAR?");
        if (a == JOptionPane.YES_OPTION) {
            MenuProductos m = new MenuProductos();
            m.setVisible(true);
            this.dispose();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed
    private boolean validacion() {
        try {
            if(jtxtID.getText().length()>6){
            JOptionPane.showMessageDialog(null,"el Id no puede tener mas de 6 digitos");
            return false;
            }
            else{
            double id = Double.parseDouble(jtxtprec.getText());
            int cant = Integer.parseInt(jtxtcant.getText());
            if (jtxtUM.getText().isEmpty() || jtxtcant.getText().isEmpty() || jtxtnom.getText().isEmpty() || jtxtprec.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Algún campo esta vacío");
                return false;
            } else {
                return true;
            }
            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "EL campo precio y el campo cantidad solo aceptan números");
            return false;
        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> comboActualizar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField jtxtID;
    private javax.swing.JTextField jtxtUM;
    private javax.swing.JTextField jtxtcant;
    private javax.swing.JTextField jtxtnom;
    private javax.swing.JTextField jtxtprec;
    // End of variables declaration//GEN-END:variables
}
