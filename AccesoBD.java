package proyectois;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class AccesoBD {
     private static Connection con;
    // Declaramos los datos de conexion a la bd
    private static final String driver="com.mysql.jdbc.Driver";
    private static final String user="root";
    private static final String pass="12345";
    private static final String url="jdbc:mysql://localhost:3306/proyectoSI";
    //MÉTODO PARA CONECTARSE A LA BASE DE DATOS MySQL

    public AccesoBD() {
      con=null;
        try{
            Class.forName(driver);
            // Nos conectamos a la bd
            con= (Connection) DriverManager.getConnection(url, user, pass);
            // Si la conexion fue exitosa mostramos un mensaje de conexion exitosa
            if (con!=null){
            }
        }
        // Si la conexion NO fue exitosa mostramos un mensaje de error
        catch (ClassNotFoundException | SQLException e){
             JOptionPane.showMessageDialog(null,"Error de conexion" + e);}
    }
     
    //MÉTODO PARA ACTUALIZAR LA BASE DE DATOS
    public void ingresaBD(String sql){
    try{
            Statement stm=con.createStatement();
            stm.executeUpdate(sql);
            JOptionPane.showMessageDialog(null,"Ingreso exitoso");
        }catch(SQLException e){
            System.out.println("Error de actualización! :( "+e.toString());            
        }
    }
    public void actualizaBD(String sql){
        try{
            Statement stm=con.createStatement();
            stm.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "Actualización exitosa");
        }catch(SQLException e){
            System.out.println("Error de actualización! :( "+e.toString());            
        }
    }
    public void eliminaBD(String sql){
        try{
            Statement stm=con.createStatement();
            stm.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "Eliminación exitosa");
        }catch(SQLException e){
            System.out.println("Error de actualización! :( "+e.toString());            
        }
    }
    //MÉTODO PARA CONSULTAR EN LA BASE DE DATOS
    public ResultSet consultaBD(String sql)throws Exception{
        Statement stm=con.createStatement();
        ResultSet cursor;
        cursor=stm.executeQuery(sql);        
        return cursor;
    }
    public ArrayList<String> llenarcombos(String sql,String campo) throws Exception{
        ArrayList<String> lista = new ArrayList<String>();
        ResultSet resultado=consultaBD(sql);
        try {
        while(resultado.next())
        {
        lista.add(resultado.getString(campo));
        }
        } catch (Exception e) {
        }
        return lista;
    }
    //MÉTODO PARA CERRAR LA BASE DE DATOS
    public void cerrarBD(){
        try{
            con.close();
            System.out.println("Conexión cerrada! :3 ");
        }catch(SQLException e){
            System.out.println("No se puede cerrar la conexion! >:c "+e.toString());            
        }        
    }        
}
